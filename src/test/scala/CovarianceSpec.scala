import org.scalatest.flatspec._
import org.scalatest.matchers.should._

class CovarianceSpec extends AnyFlatSpec with Matchers {

  "Projector for StarredWordLine" should "work with Page[StarredWordLine], not with Page[WordLine]" in {
    val starredWordLineProjector = new Projector[StarredWordLine](StarredConverter)

    val starredWordLinePage = new HelloPage[StarredWordLine](List())
    val wordLinePage = new HelloPage[WordLine](List())

    "starredWordLineProjector.project(starredWordLinePage)" should compile
    "starredWordLineProjector.project(wordLinePage)" shouldNot compile
  }

  "Projector for WordLine" should "work with Page[WordLine] and Page[StarredWordLine]" in {
    val wordLineProjector = new Projector[WordLine](LineConverter)

    val starredWordLinePage = new HelloPage[StarredWordLine](List())
    val wordLinePage = new HelloPage[WordLine](List())

    "wordLineProjector.project(starredWordLinePage)" should compile
    "wordLineProjector.project(wordLinePage)" should compile
  }

  "Projector for StarredWordLine" should "work with Converter[StarredWordLine] and Converter[WordLine]" in {
    "val starredWordLineProjector = new Projector[StarredWordLine](StarredConverter)" should compile
    "val starredWordLineProjector = new Projector[StarredWordLine](LineConverter)" should compile
  }

  "Projector for WordLine" should "work with Converter[WordLine], not with Converter[StarredWordLine]" in {
    "val wordLineProjector = new Projector[WordLine](StarredConverter)" shouldNot compile
    "val wordLineProjector = new Projector[WordLine](LineConverter)" should compile
  }

}
