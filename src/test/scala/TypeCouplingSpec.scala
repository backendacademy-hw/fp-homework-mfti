import org.scalatest.flatspec._
import org.scalatest.matchers.should._


class TypeCouplingSpec extends AnyFlatSpec with Matchers {
  "Greeter" should "use only specified language" in {

    val greeter = new RussianGreeter
    val greetingOne = new FirstRussianGreeting {}
    val greetingTwo = new SecondRussianGreeting {}
    val greetingThree = new Greeting[English.type] {
      override def text: String = "Hello!"
    }

    val stream = new java.io.ByteArrayOutputStream()
    Console.withOut(stream) {
      greeter.greet(greetingOne)
      greeter.greet(greetingTwo)
    }

    stream.toString shouldEqual "Категорически приветствую!\nМое почтение!\n"
    "greeter.greet(greetingThree)" shouldNot compile
  }
}
