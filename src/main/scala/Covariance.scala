trait Converter[-S] {
  def convert(value: S): String
}

trait Page[+R] {
  def read: (Option[R], Page[R])
}

class Projector[R](converter: Converter[R]) {
  def project(screen: Page[R]): String = screen.read._1 match {
    case Some(value) => converter.convert(value)
    case None => ""
  }
}

class WordLine(val word: String)
class StarredWordLine(val stars: Int, word: String) extends WordLine(word)

object LineConverter extends Converter[WordLine] {
  override def convert(value: WordLine): String = value.word + "\n"
}

object StarredConverter extends Converter[StarredWordLine] {
  override def convert(value: StarredWordLine): String = "*" * value.stars + value.word + "*" * value.stars + "\n"
}

class HelloPage[R <: WordLine](lines: Seq[R]) extends Page[R] {
  override def read: (Option[R], Page[R]) = {
    if (lines.isEmpty) (None, this)
    else (Some(lines.head), new HelloPage[R](lines.tail))
  }
}