/**
 * Работоспособность проверяется в приложенном тесте
 */

trait Language

object Russian extends Language
object English extends Language

trait Greeting[L <: Language] {
  def text: String
}

class Greeter[L <: Language] {
  def greet(greetings: Greeting[L]): Unit = println(greetings.text)
}

trait FirstRussianGreeting extends Greeting[Russian.type] {
  override def text: String = "Категорически приветствую!"
}

trait SecondRussianGreeting extends Greeting[Russian.type] {
  override def text: String = "Мое почтение!"
}

class RussianGreeter extends Greeter[Russian.type]